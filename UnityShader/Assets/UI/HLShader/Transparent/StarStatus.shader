// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "HL/Transparent/StarStatus"
{
	Properties
	{
		_MainTex("Main Tex", 2D) = "white" {}

	[HideInInspector][Enum(UnityEngine.Rendering.BlendOp)] _BlendOp("Blend Op", float) = 0 //Add
		[HideInInspector][Enum(UnityEngine.Rendering.BlendMode)] _BlendSrc("Blend Src Factor", float) = 5  //SrcAlpha
		[HideInInspector][Enum(UnityEngine.Rendering.BlendMode)] _BlendDst("Blend Dst Factor", float) = 10 //OneMinusSrcAlpha
		[HideInInspector][Enum(UnityEngine.Rendering.CullMode)] _CullMode("Cull Mode", float) = 2 //Back
		[HideInInspector][Enum(UnityEngine.Rendering.CompareFunction)] _ZTest("Z Test", float) = 4 //LessEqual
		[HideInInspector][Enum(Off, 0, On, 1)] _ZWrite("Z Write", float) = 0 //Off
	}

		CGINCLUDE
		ENDCG

		SubShader
	{
		Tags{ "Queue" = "Transparent-1" }
		Pass
	{
		Tags{ "LIGHTMODE" = "Always" }
		Lighting Off
		Fog{ Mode Off }
		BlendOp[_BlendOp]
		Blend[_BlendSrc][_BlendDst]
		Cull[_CullMode]
		ZTest[_ZTest]
		ZWrite[_ZWrite]

		CGPROGRAM
#include "../HLInclude.cginc"
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
#pragma target 2.0

		sampler2D StatusTex;

	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
		float2 uv2 : TEXCOORD1;
		float2 uv3 : TEXCOORD2;
	};

	struct v2f
	{
		float4 pos : SV_POSITION;
		float4 uv : TEXCOORD0;
		float2 uv2 : TEXCOORD1;
	};

	v2f vert(appdata i)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(i.vertex);
		o.uv = float4(i.uv, i.uv2);
		o.uv2 = i.uv3;
		return o;
	}

	fixed4 frag(v2f i) : COLOR0
	{
		fixed4 color = tex2D(_MainTex, i.uv.xy);
		fixed4 color2 = tex2D(_MainTex, i.uv.zw);

		float u = frac(i.uv2.x);
		float channelValue = i.uv2.x - u;
		float a = saturate(channelValue - 0.0f)*saturate(2.0f - channelValue);
		float b = saturate(channelValue - 2.0f)*saturate(4.0f - channelValue);
		float c = saturate(channelValue - 4.0f)*saturate(6.0f - channelValue);
		float d = saturate(channelValue - 6.0f)*saturate(8.0f - channelValue);
		fixed4 status = tex2D(StatusTex, float2(u,i.uv2.y));
		fixed statusResult = dot(status, fixed4(a, b, c, d));

		//clip(statusResult - 0.9); //compatibility problem in some XiaoMi phone
		//color.a *= color2.a;
		color.a *= color2.a * statusResult;

		return color;
	}

		ENDCG
	}
	}
		//Fallback "VertexLit"
}

// jerryxzhang todo
//float4 uvs[9] = {};
//float type = vertex.z;
//int typeIdx = (int)type;
//float4 texUv = uvs[typeIdx];
//fixed4 color = tex2D(_MainTex, texUv.xy);
//fixed4 color2 = tex2D(_MainTex, texUv.zw);
