﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseShadowMap : MonoBehaviour
{
	public GameObject SceneNode ; 
	public GameObject LightNode;
	public RenderTexture DepthTexture;
	public Shader CaptureDepthShader;

	private Camera LightCamera;
	
	void Awake()
	{
		if (SceneNode == null || LightNode == null || DepthTexture == null) 
		{
			Debug.LogError (" input scene is miss");
			return;
		}

		if (CaptureDepthShader == null) 
		{
			CaptureDepthShader = Shader.Find("ShadowMap/CaptureDepth");
		}
	}

	void Start () 
	{
		//1. Create 
		LightCamera = CreateCamera (LightNode,DepthTexture);
		//2. Set
		SetOrthoCamera (SceneNode, LightNode, LightCamera);
		//3. Capture 
		CaptureDepth (LightCamera);
		//4. 设置阴影相机投影矩阵
		GetLightProjectMatrix (LightCamera);
	}

	//方向：与光源方向相同
	//位置：对于方向光，应该重置其位置信息（这里直接使用了光源位置信息）
	public  Camera CreateCamera(GameObject lightObject, RenderTexture rt)
	{
		GameObject cameraObject = new GameObject();

		cameraObject.name = "Light Camera";
		cameraObject.transform.SetParent(lightObject.transform);
		cameraObject.transform.localPosition = Vector3.zero;
		cameraObject.transform.localRotation = Quaternion.identity;

		Camera camera = cameraObject.AddComponent<Camera>();
		camera.targetTexture = rt;
		camera.clearFlags = CameraClearFlags.Color;
		camera.backgroundColor = Color.white;
		camera.orthographic = true;
		camera.enabled = false;

		Shader.SetGlobalTexture("_LightDepthTex", rt);

		return camera;
	}

	void CaptureDepth(Camera lightCamera)
	{
		lightCamera.RenderWithShader(CaptureDepthShader, "RenderType");
	}
	
	void SetOrthoCamera(GameObject scene ,GameObject light , Camera camera)
	{
		//1. Scene AABB 
		Bounds sceneBounds = Utils.GetSceneBounds (scene);
		if (sceneBounds.size.x == 0) 
		{
			return;
		}
		debugSceneAABB = sceneBounds;

		//2. Convert to Light Space (转换到光源本地坐标)
		sceneBounds = Utils.ConvertBounds(sceneBounds, light.transform.worldToLocalMatrix);

		//3. Set
		camera.transform.localRotation = Quaternion.identity ;
		camera.transform.localPosition = new Vector3(sceneBounds.center.x,sceneBounds.center.y,sceneBounds.center.z);
		camera.orthographicSize = Mathf.Max(sceneBounds.size.x / 2, sceneBounds.size.y / 2);
		camera.nearClipPlane = sceneBounds.min.z - camera.transform.localPosition.z;
		camera.farClipPlane = sceneBounds.max.z- camera.transform.localPosition.z;;
	}

	Bounds debugSceneAABB;
	void OnDrawGizmos() 
	{
		Gizmos.color = new Color(1, 0, 1, 1F);
		Gizmos.DrawWireCube(debugSceneAABB.center, debugSceneAABB.size);
		Gizmos.color = new Color(1, 0, 0, 0.5F);
		Gizmos.DrawWireCube (debugSceneAABB.center,new Vector3(1,1,1));
	}

	//perspective matrix
	void  GetLightProjectMatrix(Camera camera)
	{
//		Matrix4x4 posToUV = new Matrix4x4();
//		posToUV.SetRow(0, new Vector4(0.5f,    0,    0, 0.5f));
//		posToUV.SetRow(1, new Vector4(   0, 0.5f,    0, 0.5f));
//		posToUV.SetRow(2, new Vector4(   0,    0,    1,    0));
//		posToUV.SetRow(3, new Vector4(   0,    0,    0,    1));

		Matrix4x4 worldToView = camera.worldToCameraMatrix;
		Matrix4x4 projection  = GL.GetGPUProjectionMatrix(camera.projectionMatrix, false);

		Matrix4x4 lightProjecionMatrix =  projection * worldToView;

		Shader.SetGlobalMatrix ("_LightProjection", lightProjecionMatrix);
	}



}
