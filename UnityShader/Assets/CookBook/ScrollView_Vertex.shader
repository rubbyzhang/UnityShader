﻿Shader "Custom/ScrollViewUV_Vertex" 
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,0)
		_SpecColor("Spec Color", Color) = (1,1,1,1)
		_Emission("Emmisive Color", Color) = (0,0,0,0)
		_Shininess("Shininess", Range(0.01, 1)) = 0.7

		_MainTex("Base (RGB)", 2D) = "white" {}
		_NormalTex("Normal", 2D) = "white" {}

		_ScrollXSpeed("XSpeed", Range(0,10)) = 0
		_ScrollYSpeed("YSpeed", Range(0,10)) = 0.3
	}

	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}  

		ZWrite off
		AlphaTest Greater 0 
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			Material
			{
				Diffuse[_Color]
				Ambient[_Color]
				Shininess[_Shininess]
				Specular[_SpecColor]
				Emission[_Emission]
			}

			Lighting Off
			SeparateSpecular On

			SetTexture[_MainTex]
			{
				Combine texture *	primary , texture * primary 
			}

			//SetTexture[_MainTex]
			//{
			//	constantColor[_Color]
			//	Combine previous *	constant
			//}
		}
	}
}