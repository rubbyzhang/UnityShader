// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "HL/Other/DepthRender" {
	SubShader {
		Tags { "RenderType"="Opaque" "DepthMode"="true"}
		Pass {
			Fog { Mode Off }
			Blend Off
			Lighting Off
			Cull Back
			ZWrite On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos : SV_POSITION;
				float2 depth : TEXCOORD0;
			};

			v2f vert (appdata_base v) {
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				//UNITY_TRANSFER_DEPTH(o.depth);
				o.depth = o.pos.zw;
				return o;
			}

			fixed4 frag(v2f i) : COLOR {
				//UNITY_OUTPUT_DEPTH(i.depth);

				float depth = min(i.depth.x, 199.99f);
				fixed4 enc = fixed4(0,0,0,0);
				enc.rg = EncodeFloatRG(depth * 0.005f);
				return enc;
			}
			ENDCG
		}
	}
}