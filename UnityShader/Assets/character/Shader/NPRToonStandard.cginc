// UNITY_SHADER_NO_UPGRADE
#ifndef NPRTOONSTANDARD_INCLUDED
#define NPRTOONSTANDARD_INCLUDED

#include "HLSLSupport.cginc"
#include "UnityShaderVariables.cginc"
#include "UnityCG.cginc"
#include "Lighting.cginc"
#include "AutoLight.cginc"
#include "NPRToonLighting.cginc"

#define NPRTOON_VS_COMMON(type, o) \
	type o;                                                      \
	UNITY_INITIALIZE_OUTPUT(type, o);                            \
	o.pos = mul(UNITY_MATRIX_MVP, v.vertex);                     \
    o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex);               \
    float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;          \
    fixed3 worldNormal = UnityObjectToWorldNormal(v.normal);     \
    o.worldNormal = normalize(worldNormal);                      \
    o.worldCamDir = normalize(UnityWorldSpaceViewDir(worldPos)); \
    o.worldPos = worldPos;                                       \
    o.vlight.rgb = ToonAmbientLighting(o.worldNormal);           \
    o.vlight.a = v.color.a;                                      \
    TRANSFER_SHADOW(o);

#define NPRTOON_PS_COMMON \
	float2 shadow   = LIGHT_ATTENUATION(IN);                     \
    half4 shading   = tex2D(_ShadingMask, IN.uv.xy);             \
    shading.g       = shading.g * 0.5f;                          \
    half4 diffuse   = tex2D(_MainTex, IN.uv.xy);                 \
	float3 lightDir = CHARACTER_SUNLIGHTDIR.xyz;                 \
    float3 normal   = normalize(IN.worldNormal);                 \
    float3 viewDir  = IN.worldCamDir;                            \
    diffuse.rgb     = CombineDecal(IN.uv.xy, diffuse.rgb);       \
    float3 lighting = ToonBaseLighting(normal, lightDir, shading.rgb, CharacterSunLightColor.rgb, shadow); \
    float4 rim      = 0;                                         \
    float3 specular = float3(0, 0, 0);                           \

struct v2f_standard 
{
    float4 pos : SV_POSITION;
    half4  uv: TEXCOORD0;
    half3  worldNormal: TEXCOORD1;
    float3 worldPos: TEXCOORD2;
    float3 worldCamDir: TEXCOORD3;
    float4 vlight: TEXCOORD4;
    LIGHTING_COORDS(5, 6)
};

struct v2f_anisotropy 
{
    float4 pos : SV_POSITION;
    half4  uv: TEXCOORD0;
    half3  worldNormal: TEXCOORD1;
    float3 worldPos: TEXCOORD2;
    float3 worldCamDir: TEXCOORD3;
    float4 vlight: TEXCOORD4;
    float3 tangent: TEXCOORD5;
    LIGHTING_COORDS(6, 7)
};

// global properties
float _Fade;
fixed4 _Color;

// common properties
sampler2D _MainTex;
sampler2D _ShadingMask;
float4 _MainTex_ST;
float4 _ShadingMask_ST;
samplerCUBE _GlossyCube;

// decal related properties
sampler2D _DecalTex;
float4 DecalUV;

// rim lighting related properties
float RimGeneral;
float RimPower;
float4 RimColor;

// specular related properties    
float SpecularPower;
float SpecularFactor;
float4 SpecularColor;

inline float GetMipFromRoughness(float roughness)
{
	float level = 3 - 1.15 * log2(roughness);
	return 9.0 - 1 - level;
}

inline fixed3 CombineDecal(float2 uv, float3 diffuse)
{
#if USE_DECAL
    half2 uv01 = frac(uv.xy);
    half2 decalUV = (uv01 - DecalUV.xy) * DecalUV.zw + half2(0.5f, 0.5f);
    decalUV.xy = saturate(decalUV.xy);
    fixed4 diffuseDecal = tex2D(_DecalTex, decalUV);
    return lerp(diffuse.rgb, diffuseDecal.rgb, diffuseDecal.a);
#else
	return diffuse.rgb;
#endif
}

inline fixed4 CombineLighting(float4 albedo, float3 ambient, float3 base, float4 rim, float3 specular)
{
	base = ambient + base;
	base = (lerp(albedo.xyz, rim.rgb, rim.a) + specular) * base;

#if defined(USE_ALPHABLEND)
    return fixed4(base, _Fade * albedo.a);
#else
	return fixed4(base, 1);
#endif
}

// vertex shader
v2f_standard NPRToonCharacterStandardVS(appdata_full v) 
{
    NPRTOON_VS_COMMON(v2f_standard, o);
    return o;
}

// vertex shader
v2f_anisotropy NPRToonCharacterAnisotropyVS(appdata_full v) 
{
    NPRTOON_VS_COMMON(v2f_anisotropy, o);
    o.uv.zw = TRANSFORM_TEX(v.texcoord, _ShadingMask);
    o.tangent = normalize(UnityObjectToWorldNormal(v.tangent));
    return o;
}

// fragment shader for anisotropy object rendering
fixed4 NPRToonCharacterAnisotropyPS(v2f_anisotropy IN, 
	uniform float  _AnisotropyShift1, 
	uniform float  _AnisotropyShift2, 
	uniform float  _AnisotropyPower1, 
	uniform float  _AnisotropyPower2,
	uniform fixed4 _SpecularColor1,
	uniform fixed4 _SpecularColor2) : SV_Target
{
	NPRTOON_PS_COMMON;

#if defined(INVERT_NORMAL)
	normal = -normal;
#endif

#if USE_RIMLIGHTING
	rim.rgb = ToonRimColor(shading.rgb);
    rim.a   = RimColor.a * ToonRimLighting(normal, viewDir, RimGeneral, RimPower) * shadow.y * IN.vlight.a;
#endif

	// sample jitter&shift texture with tiled uv
	float4 anisotropyShading = tex2D(_ShadingMask, IN.uv.zw);
    float2 anisotropy = ToonAnisotropyLighting(IN.tangent, normal, viewDir, lightDir, anisotropyShading, 
    	float2(_AnisotropyShift1, _AnisotropyShift2), float2(_AnisotropyPower1, _AnisotropyPower2), shadow.y);

    anisotropy *= float2(_SpecularColor1.a, _SpecularColor2.a) * CharacterSunLightColor.a * IN.vlight.a;

    // special combine formula for anisotropy specular lighting
	specular = (anisotropy.x * _SpecularColor1 + anisotropy.y * _SpecularColor2);
	fixed4 o = CombineLighting(diffuse, IN.vlight.rgb, lighting, rim, specular);

	return fixed4(lerp(o.rgb, specular, saturate(anisotropy.x + anisotropy.y)), o.a);
}

// fragment shader for general character rendering 
fixed4 NPRToonCharacterStandardPS(v2f_standard IN, uniform float _Roughness): SV_Target 
{
	NPRTOON_PS_COMMON;

#if USE_RIMLIGHTING
	rim.rgb = ToonRimColor(shading.rgb);
    rim.a   = RimColor.a * ToonRimLighting(normal, viewDir, RimGeneral, RimPower);
#endif

#if USE_SPECULARLIGHTING
    specular = ToonSpecularLighting(normal, viewDir, lightDir, SpecularPower, SpecularFactor, shading.rgb, SpecularColor);
#endif

	fixed4 result = CombineLighting(diffuse, IN.vlight.rgb, lighting, rim, specular);

#if USE_GLOSSYREFLECTION
	fixed3 glossyRefl = texCUBE(_GlossyCube, reflect(-viewDir, normal)).rgb;
	result.rgb = lerp(result.rgb, glossyRefl * 2.0f, shading.a);
#endif

	return result;
}

fixed4 NPRToonCharacterStandardPointPS(v2f_standard IN): SV_Target 
{
	half4 shading = tex2D(_ShadingMask, IN.uv.xy);

	float3 N = normalize(IN.worldNormal);
	float3 V = normalize(_WorldSpaceCameraPos.xyz - IN.worldPos.xyz);

    UNITY_LIGHT_ATTENUATION(atten, IN, IN.worldPos.xyz)
    atten = saturate(dot(N,V) * 0.5 + 0.5) *  min(atten, shading.r);

    return smoothstep(0, 1.0f, atten * _LightColor0) ;
}

#endif