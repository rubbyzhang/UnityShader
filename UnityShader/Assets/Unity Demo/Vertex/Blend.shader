﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Blend"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
	}

	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }

		Pass
		{
			ZWrite Off
		//	Blend SrcAlpha OneMinusSrcAlpha
			//Blend DstColor SrcColor  
			Blend DstColor Zero , SrcAlpha OneMinusSrcAlpha
		//Blend One One
		//BlendOp Sub


			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			float4 vert(float4 vertex : POSITION) : SV_POSITION
			{
				return UnityObjectToClipPos(vertex);
			}

			fixed4 _Color;
			fixed4 frag() : SV_Target
			{
				return _Color;
			}
		ENDCG
		}
	}
}
