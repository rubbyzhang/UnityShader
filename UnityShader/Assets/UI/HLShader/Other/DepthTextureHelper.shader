// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "HL/Other/DepthTextureHelper" {
Properties {
	//_MainTex ("Base (RGB)", 2D) = "white" {}
	_ProjectionParams11 ("_ProjectionParams11", Vector) = (1,2,3,4)
	_ScreenParams11 ("_ScreenParams11", Vector) = (1,2,3,4)
}

SubShader {
	Pass {
		Lighting Off
		Fog { Mode Off }
		Cull Off
		ZTest Always
		ZWrite Off

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
#pragma target 2.0
#include "UnityCG.cginc"

//uniform sampler2D _MainTex;
uniform sampler2D _CameraDepthTexture;

struct appdata {
    float4 vertex : POSITION;
    half2 texcoord : TEXCOORD0;
};

struct v2f {
	float4 pos : SV_POSITION;
	half2 uv : TEXCOORD0;
	float4 pos2 : TEXCOORD1;
};

v2f vert( appdata v )
{
	v2f o;
	o.pos = UnityObjectToClipPos (v.vertex);
	o.uv = MultiplyUV( UNITY_MATRIX_TEXTURE0, v.texcoord );
	o.pos2 = ComputeScreenPos(o.pos);
	return o;
}

fixed4 frag (v2f i) : SV_Target
{
	//fixed4 orig = tex2D(_MainTex, i.uv);
	
	float depth = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.pos2));
	float depthEye = DECODE_EYEDEPTH(depth);
	
	float4 enc = EncodeFloatRGBA(depthEye * 0.001f);
	return enc;
}

ENDCG

	}
}

Fallback off

}