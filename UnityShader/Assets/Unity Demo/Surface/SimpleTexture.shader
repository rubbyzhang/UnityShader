﻿Shader "Custom/SimpleTexture" 
{
	Properties 
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("BumpMap" , 2D) = "bump" {}
		_DetailTex("DetailTex" , 2D) = "while" {}
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 2.0
		#include "UnityCG.cginc"

		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _DetailTex;

		fixed4 _Color;

		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float2 uv_DetailTex;
		};


		void surf (Input IN, inout SurfaceOutput o) 
		{
			float4 t1 = tex2D(_MainTex, IN.uv_MainTex);
			float4 t2 = tex2D(_DetailTex, IN.uv_DetailTex);
			
			o.Albedo = t1.rgb * t2.rgb   * _Color.rgb * 2;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		}
		ENDCG
	}
	FallBack "Diffuse"
}
