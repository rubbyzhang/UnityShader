﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Red" 
{
	SubShader
	{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry" }
		Pass
		{
				Stencil
				{
					Ref 2
					Comp always
					Pass replace
				}

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				struct v2f 
				{
					float4 pos : SV_POSITION;
				};
				float4 vert(float4 vertex : POSITION) : SV_POSITION
				{
					return UnityObjectToClipPos(vertex);
				}

				half4 frag(v2f i) : SV_Target
				{
					return half4(1,0,0,1);
				}
			ENDCG
		}
	}
}