﻿Shader "Custom/DetailTextureInScreen"
{
	Properties
	{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_DetailTex("DetailTex" , 2D) = "while" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 2.0
		#include "UnityCG.cginc"

		sampler2D _MainTex;
		sampler2D _DetailTex;

		struct Input
		{
			float2 uv_MainTex;
			float4 screenPos;
		};

		void surf(Input IN, inout SurfaceOutput o)
		{
			float4 t1 = tex2D(_MainTex, IN.uv_MainTex);

			//相当于整个问题处于屏幕中
			float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
			float4 t2 = tex2D(_DetailTex, screenUV);

			o.Albedo = t1.rgb * t2.rgb * 2;
		}
		ENDCG
	}

	FallBack "Diffuse"
}
