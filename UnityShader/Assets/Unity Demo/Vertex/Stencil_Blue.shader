﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Blue" {
	SubShader{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry+2" }
		Pass{
		Stencil{
		Ref 1
		Comp equal
	}

		CGPROGRAM
#include "UnityCG.cginc"
#pragma vertex vert
#pragma fragment frag

	struct v2f {
		float4 pos : SV_POSITION;
	};
	float4 vert(float4 vertex : POSITION) : SV_POSITION
	{
		return UnityObjectToClipPos(vertex);
	}
	half4 frag(v2f i) : SV_Target{
		return half4(0,0,1,1);
	}
		ENDCG
	}
	}
}