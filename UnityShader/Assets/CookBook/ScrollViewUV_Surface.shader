﻿Shader "Custom/ScrollViewUV_Surface" 
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_ScrollXSpeed("XSpeed", Range(0,10)) = 2
		_ScrollYSpeed("YSpeed", Range(0,10)) = 2
	}

	SubShader 
	{
		Tags 
		{ "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 3.0

		sampler2D _MainTex;
		struct Input 
		{
			float2 uv_MainTex;
		};

		fixed4 _Color;
		float _ScrollXSpeed ;
		float _ScrollYSpeed ;

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float x  = frac(IN.uv_MainTex.x + _ScrollXSpeed  * _Time.x) ;
			float y  = frac(IN.uv_MainTex.y + _ScrollYSpeed  * _Time.y) ;

			fixed4 c = tex2D (_MainTex, fixed2(x,y)) * _Color;
			o.Albedo = c.rgb;
			o.Alpha  = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
