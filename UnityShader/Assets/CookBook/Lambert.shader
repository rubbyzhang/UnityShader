﻿Shader "Custom/Lambert" 
{
	Properties
	{
		_EmissionColor ("Emission Color", Color) = (1,1,1,1)
		_DiffuseColor ("Diffuse Color" , Color) = (1,1,1,1)
		_SpecularColor ("Specular Color" , Color) = (1,1,1,1)
		_Smoothing ("Specular Smoothing " , Range(0,1)) = 0.1
		_Gloss ("Specular Gloss " , Range(0,10)) = 0.1
		_MainTex("Main Texture", 2D) = "while" {} 
	}
	SubShader
	 {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BaseSpecular
		#pragma target 2.0

		struct Input 
		{
			float color : COLOR ;
			float2 uv_MainTex ;
		};

		float4  _EmissionColor ;
		float4  _DiffuseColor ;
		float4  _SpecularColor ;
		float _Smoothing ;
		float _Gloss ;

		sampler2D _MainTex ;

		void surf (Input IN, inout SurfaceOutput o) 
		{
			//o.Emission = _EmissionColor.rgb;
			//o.Albedo   =  _DiffuseColor.rgb;

			o.Albedo = _DiffuseColor.rgb * tex2D(_MainTex , IN.uv_MainTex) ;
			o.Alpha    = _EmissionColor.a ;
		}

		//inliene half4  Lighting<Name>(SurfaceOutput s , half3 LightDir , half atten)
		//inline half4 Lighting<Name>(SurfaceOutput s , half3 LightDir , half3 viewDir , half atten)
		//inline half4 Lighting<Name>_PrePass()SurfaceOutput s , half4 LightDir)

		//在这个模型中 Emission 为啥会影响实际输出呢？
		inline half4 LightingBaseLambert(SurfaceOutput s , half3 lightDir , half atten)
		{
			float difLight = max(0, dot(s.Normal , lightDir));
			float4 col ;
			col.rgb = s.Albedo * _LightColor0.rgb * difLight  * atten  ;
			col.a = s.Alpha ;
			return col ;
		}

		//halfLambert 提高亮度，其实是线性拉伸
		inline half4 LightingHalfLambert(SurfaceOutput s , half3 lightDir , half atten)
		{
			float difLight = max(0, dot(s.Normal , lightDir));
			difLight = difLight * 0.5  + 0.5 ;
			float4 col ;
			col.rgb = s.Albedo * _LightColor0.rgb * difLight  * atten  ;
			col.a = s.Alpha ;
			return col ;
		}

		//Ramp Texture 控制反射颜色 ,   为啥球体的背面是白色，保持本身的颜色吗？-->注意材质的使用方式 repeat或者wrap
		inline half4 LightingRampLambert(SurfaceOutput s , half3 lightDir , half atten)
		{
			float difLight =max(0,dot(s.Normal , lightDir)) ;
			float3 rampColor = tex2D(_MainTex , float2(difLight,difLight)).rgb;
			float4 col ;
			col.rgb = s.Albedo * _LightColor0.rgb * rampColor   ;
			col.a = s.Alpha ;
			return col ;
		}

		//添加环境光
		inline half4 LightingBaseDiffuse(SurfaceOutput s , half3 lightDir , half atten)
		{
			float difLight = max(0, dot(s.Normal , lightDir));
			float4 col ;
			col.rgb = (s.Emission.rgb * _LightColor0.rgb + s.Albedo * _LightColor0.rgb * difLight  ) * atten  ;
			col.a = s.Alpha ;
			return col ;
		}

		//利用ViewDir模拟DRBF
		inline half4 LightingSimulateDRBF(SurfaceOutput s , half3 lightDir , half3 viewDir , half atten)
		{
			float difLight  = max(0,dot(s.Normal , lightDir)) ;
			float viewLight = max(0,dot(s.Normal , viewDir)) ;

			float3 rampColor = tex2D(_MainTex , float2(difLight,viewLight)).rgb;

			float4 col ;
			col.rgb = s.Albedo * _LightColor0.rgb * rampColor   ;
			col.a = s.Alpha ;
			return col ;
		}

		inline half4 LightingBaseSpecular(SurfaceOutput s , half3 lightDir , half3 viewDir , half atten)
		{
			half3 h = normalize(viewDir + lightDir) ;
			half diff = max(0 , dot(lightDir , s.Normal)) ;
			float hn = max(0 , dot(h , s.Normal)) ;
			float spec = pow(hn, _Smoothing * 128) * _Gloss;

			half4 col ;
			col.rgb = _SpecularColor * _LightColor0.rgb  * spec ;
			col.a = s.Alpha ;
			return col ;				
		}


		ENDCG
	}
	FallBack "Diffuse"
}
