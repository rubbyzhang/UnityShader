// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge Beta 0.36 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.36;sub:START;pass:START;ps:flbk:,lico:0,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:False,hqlp:False,tesm:0,blpr:5,bsrc:3,bdst:1,culm:0,dpts:2,wrdp:True,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|emission-29-OUT,clip-249-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33665,y:32616,ptlb:MainTex,ptin:_MainTex,tex:14b8d76bfa60c1741bf18337f5c6524a,ntxv:0,isnm:False|UVIN-231-UVOUT;n:type:ShaderForge.SFN_Color,id:3,x:33584,y:32438,ptlb:MainColor,ptin:_MainColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:4,x:33128,y:32397;n:type:ShaderForge.SFN_Multiply,id:5,x:33304,y:32435|A-3-RGB,B-2-RGB;n:type:ShaderForge.SFN_Multiply,id:6,x:33356,y:32691|A-3-A,B-2-A;n:type:ShaderForge.SFN_Multiply,id:7,x:33067,y:32578|A-4-RGB,B-5-OUT;n:type:ShaderForge.SFN_Multiply,id:8,x:33115,y:33066|A-4-A,B-6-OUT;n:type:ShaderForge.SFN_Tex2d,id:27,x:33561,y:32962,ptlb:NoiseTex,ptin:_NoiseTex,tex:9710d323997a9754ead4c87934c48de1,ntxv:0,isnm:False|UVIN-226-UVOUT;n:type:ShaderForge.SFN_OneMinus,id:28,x:33369,y:32913|IN-27-RGB;n:type:ShaderForge.SFN_Multiply,id:29,x:33073,y:32864|A-253-OUT,B-28-OUT;n:type:ShaderForge.SFN_Panner,id:226,x:33828,y:32912,spu:0,spv:1|DIST-236-OUT;n:type:ShaderForge.SFN_Time,id:228,x:34633,y:32672;n:type:ShaderForge.SFN_ValueProperty,id:229,x:34631,y:33019,ptlb:NoiseSpeed,ptin:_NoiseSpeed,glob:False,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:230,x:34393,y:33166,ptlb:NoiseAng,ptin:_NoiseAng,glob:False,v1:0;n:type:ShaderForge.SFN_Panner,id:231,x:33847,y:32476,spu:0,spv:1|DIST-235-OUT;n:type:ShaderForge.SFN_ValueProperty,id:233,x:34396,y:32465,ptlb:MainAng,ptin:_MainAng,glob:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:234,x:34642,y:32402,ptlb:MainSpeed,ptin:_MainSpeed,glob:False,v1:1;n:type:ShaderForge.SFN_Frac,id:235,x:34165,y:32582|IN-239-OUT;n:type:ShaderForge.SFN_Frac,id:236,x:34431,y:32844|IN-237-OUT;n:type:ShaderForge.SFN_Multiply,id:237,x:34594,y:32844|A-228-T,B-229-OUT;n:type:ShaderForge.SFN_TexCoord,id:238,x:34695,y:32490,uv:0;n:type:ShaderForge.SFN_Multiply,id:239,x:34396,y:32556|A-234-OUT,B-228-TSL;n:type:ShaderForge.SFN_Cubemap,id:243,x:33570,y:32183,ptlb:cube,ptin:_cube;n:type:ShaderForge.SFN_Multiply,id:244,x:32909,y:32370|A-243-RGB,B-7-OUT;n:type:ShaderForge.SFN_Add,id:247,x:32912,y:33172|A-8-OUT,B-250-OUT;n:type:ShaderForge.SFN_Clamp01,id:249,x:32697,y:33182|IN-247-OUT;n:type:ShaderForge.SFN_ValueProperty,id:250,x:33277,y:33325,ptlb:alpha_add,ptin:_alpha_add,glob:False,v1:0.3;n:type:ShaderForge.SFN_Multiply,id:253,x:32702,y:32530|A-244-OUT,B-254-OUT;n:type:ShaderForge.SFN_ValueProperty,id:254,x:32885,y:32580,ptlb:Str,ptin:_Str,glob:False,v1:1;proporder:2-3-27-229-230-233-234-243-250-254;pass:END;sub:END;*/

Shader "Shader Forge/sand_oneface" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _MainColor ("MainColor", Color) = (1,1,1,1)
        _NoiseTex ("NoiseTex", 2D) = "white" {}
        _NoiseSpeed ("NoiseSpeed", Float ) = 0.5
        _NoiseAng ("NoiseAng", Float ) = 0
        _MainAng ("MainAng", Float ) = 0
        _MainSpeed ("MainSpeed", Float ) = 1
        _cube ("cube", Cube) = "_Skybox" {}
        _alpha_add ("alpha_add", Float ) = 0.3
        _Str ("Str", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="Always"
            }
            Blend SrcAlpha Zero
            
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers d3d11 opengl xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _MainColor;
            uniform sampler2D _NoiseTex; uniform float4 _NoiseTex_ST;
            uniform float _NoiseSpeed;
            uniform float _MainSpeed;
            uniform samplerCUBE _cube;
            uniform float _alpha_add;
            uniform float _Str;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = mul(float4(v.normal,0), unity_WorldToObject).xyz;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalDirection =  i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float4 node_4 = i.vertexColor;
                float4 node_228 = _Time + _TimeEditor;
                float2 node_342 = i.uv0;
                float2 node_231 = (node_342.rg+frac((_MainSpeed*node_228.r))*float2(0,1));
                float4 node_2 = tex2D(_MainTex,TRANSFORM_TEX(node_231, _MainTex));
                clip(saturate(((node_4.a*(_MainColor.a*node_2.a))+_alpha_add)) - 0.5);
////// Lighting: 
////// Emissive:
                float2 node_226 = (node_342.rg+frac((node_228.g*_NoiseSpeed))*float2(0,1));
                float3 emissive = (((texCUBE(_cube,viewReflectDirection).rgb*(node_4.rgb*(_MainColor.rgb*node_2.rgb)))*_Str)*(1.0 - tex2D(_NoiseTex,TRANSFORM_TEX(node_226, _NoiseTex)).rgb));
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }

    //FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
