// UNITY_SHADER_NO_UPGRADE
#ifndef NPRTOONLIGHTING_INCLUDED
#define NPRTOONLIGHTING_INCLUDED

#include "ShadingCommon.cginc"

uniform sampler2D _ToonMap;
uniform float     _LightSmoothness;
uniform float     _LightThreshold;

#define TOON_NDL(ndl) ( smoothstep( _LightThreshold - _LightSmoothness * 0.5, _LightThreshold + _LightSmoothness * 0.5, (ndl) ) )

inline half3 ToonAmbientLighting(float3 wsNormal)
{
	half3 vlight = half3(0, 0, 0);

#if UNITY_AMBIENT
  #if UNITY_SHOULD_SAMPLE_SH
    vlight = ShadeSH9(float4(result.worldNormal, 1.0));
  #endif
#else
	vlight = CharacterAmbientLightColor;
#endif

	return vlight;
}

inline half3 ToonRimColor(float3 shading)
{
	return tex2D(_ToonMap, float2(0, shading.g)).rgb;
}

inline half3 ToonBaseLighting(float3 normal, float3 lightDir, float3 shading, float3 domainColor, inout float2 shadow)
{
	shadow.x = dot(normal, lightDir);

	float ndl = max(shadow.x + (shading.r * 2 - 1), 0);
    ndl = TOON_NDL(ndl);

    shadow.x = saturate(shadow.x);
    shadow.y = min(ndl, shadow.y);

	return tex2D(_ToonMap, float2(shadow.y, shading.g + 0.5f)).rgb * domainColor;
}

inline half ToonRimLighting(float3 normal, float3 view, float threshold, float power)
{
	float edh = max(dot(view, normal), 0);
    edh = smoothstep(threshold - 0.5 * 0.5, threshold + 0.5 * 0.5, edh);

    return pow(1 - edh, power);
}

inline half3 ToonSpecularLighting(float3 normal, float3 view, float3 lightDir, float power, float factor, float3 shading, float3 domainColor)
{
	float3 h = normalize(lightDir + view);
 	float ndh = saturate(dot(normal, h));

    float spec = pow(ndh, power) * shading.b;
	return tex2D(_ToonMap, float2(spec * factor, 0.505f)).rgb * domainColor.rgb;
}

inline float StrandSpec(float3 tangent, float3 L, float3 V, float power)
{
	float3 H     = normalize(L + V);
	float  TdotH = dot(tangent, H);
	float  sinTH = sqrt(1 - TdotH * TdotH);
	return 0.25 * pow(sinTH, power) * smoothstep(-1, 0, dot(tangent, H));
}

inline float2 ToonAnisotropyLighting(float3 tangent, float3 normal, float3 view, float3 lightDir, float4 shading, float2 shift, float2 power, float shadow)
{
	float2 jitterShift = shading.ba;

#ifdef FULL_ANISOTROPYLIGHTING
	float jitter = max(0, jitterShift.x) * 0.5;
	jitterShift.y = lerp(jitter - 0.5, jitter + 0.5, jitterShift.y);
#endif

	float2 anisotropy = 0;

    // Primary specular highlight
    float3 tanA = normalize(tangent + normal * (jitterShift.y + shift.x));
    anisotropy.x = 4.0 * StrandSpec(tanA, lightDir, view, power.x) * shadow;

#ifdef FULL_ANISOTROPYLIGHTING
    // Secondary specular highlight
    float3 tanB = normalize(tangent + normal * (jitterShift.y + shift.y));
    anisotropy.y = 4.0 * StrandSpec(tanB, lightDir, view, power.y) * shadow;
#endif

    return anisotropy; 
}

#endif