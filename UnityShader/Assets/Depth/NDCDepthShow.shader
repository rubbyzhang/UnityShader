﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/NDCDepthShow"
{
    SubShader
    {
        Pass
        {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

			sampler2D _DepthTexture;

            struct uinput
            {
                float4 pos : POSITION;
                half2 uv : TEXCOORD0;
            };

            struct uoutput
            {
                float4 pos : SV_POSITION;
                half2 uv : TEXCOORD0;
            };

            uoutput vert(uinput i)
            {
                uoutput o;
                o.pos = UnityObjectToClipPos(i.pos);
                o.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, i.uv);
                o.uv.y = 1- o.uv.y ;
                return o;
            }

            fixed4 frag(uoutput o) : COLOR
            {
//    			float3 normalValues;
//				float depth;
//				DecodeDepthNormal(tex2D(_DepthTexture, o.uv), depth, normalValues);

                float depth = tex2D(_DepthTexture, o.uv) * 20;
                return fixed4(depth,depth,depth,1);
				
            }
            ENDCG
        }
	}
}