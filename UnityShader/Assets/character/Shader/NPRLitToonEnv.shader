// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "BlueWar/NPRLitToonEnv" 
{
    Properties 
    {
        [HideInInspector]_Fade("Fade", Range(0, 1)) = 1.0

        _LightThreshold("Light Threshold", Range(0, 1)) = 0.5
        _LightSmoothness("Light Smoothness", Range(0, 1)) = 0.5
        _GlowPower("Glow Power", Range(0, 1)) = 0
        _GlowColor("Glow Color", Color) = (1, 1, 1, 1)
        _OutlineColor("Outline Color", Color) = (0, 0, 0, 0)
        _Outline("Outline Width", Range(0, 10)) = 0.05
        _GlossyCube("Glossy Reflection Cube", CUBE) = "" {}
        _MainTex("Base (RGB)", 2D) = "white" {}
        _DecalTex("Decal (RGB)", 2D) = "black" {}
        _ToonMap("ToonMap (RGB)", 2D) = "white" {}
        _ShadingMask("ShadingMask (RGB)", 2D) = "white" {}
        SpecularPower("SpecularPower", Float) = 1.0
        SpecularColor("SpecularColor", Color) = (0.0, 0.0, 0.0, 0.0)
        SpecularFactor("SpecularFactor", Float) = 1.0
        RimGeneral("Rim General", Range(0, 1)) = 0.5
        RimPower("Rim Power", Float) = 1
        RimColor("RimColor", Color) = (1, 1, 1, 1)
        DecalUV("DecalUV", Vector) = (1, 1, 1, 1) //center_xy[0,0]-[1,1], tile_zw,[0,+INF] 

        _OcclusionRimColor("Occlusion Rim Color", Color) = (0, 1, 1, 1)
        _OcclusionRimPower("Occlusion Rim Power", Range(0.1, 8.0)) = 2

        [Toggle(FIXED_OUTLINESIZE)] _EnableFixedOutline("Fixed Outline Size?", Float) = 1
        [Toggle(USE_SMOOTHEDMESH)] _UseSmoothedMesh("Use Smoothed Mesh?", Float) = 0
        [Toggle(USE_AUTOLINECOLOR)] _UseAutoOutlineColor("Use Auto calculated outline color?", Float) = 1
        [Toggle(USE_DECAL)] _UseDecal("Use Decal?", Float) = 0
        [Toggle(USE_SPECULARLIGHTING)] _UseSpecularLighting("Use Specular Lighting?", Float) = 1
        [Toggle(USE_RIMLIGHTING)] _UseRimLighting("Use Rim Lighting?", Float) = 1
        [Toggle(USE_GLOSSYREFLECTION)] _UseGlossyReflection("Use Glossy Reflection?", Float) = 1
    }

    SubShader 
    {
        Tags { "RenderType" = "Opaque" "Queue" = "Geometry+401" }

        LOD 150

        /*
        Pass 
        {
        	NAME "XRAY"

            ZTest Greater
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            fixed4 _OcclusionRimColor;
            fixed _OcclusionRimPower;

            struct appdata_t {
                float4 vertex: POSITION;
                float4 normal: NORMAL;
            };

            struct v2f {
                float4 pos: SV_POSITION;
                float4 color: COLOR;
            };

            v2f vert(appdata_t v)
            {
                v2f o;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                float3 viewDir = normalize(ObjSpaceViewDir(v.vertex));
                float rim = 1 - saturate(dot(viewDir, v.normal));
                o.color = _OcclusionRimColor * pow(rim, _OcclusionRimPower);
                return o;
            }
            float4 frag(v2f i): COLOR
            {
                return i.color;
            }
            ENDCG
        }
        */

        // back-facing outline
        Pass 
        {
        	NAME "OUTLINE"

            Cull Front ZTest LEqual Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma fragmentoption ARB_precision_hint_fastest

            #pragma multi_compile FIXED_OUTLINESIZE _
            #pragma multi_compile USE_SMOOTHEDMESH _
            #pragma multi_compile USE_AUTOLINECOLOR _

            #pragma vertex vert
            #pragma fragment frag

			#include "UnityCG.cginc"
			#include "ShadingCommon.cginc"

			float _Fade;

			struct v2f 
			{
			    float4 pos: SV_POSITION;
			    float2 uv: TEXCOORD0;
			};

			sampler2D _MainTex;
			float     _Outline;
			fixed4    _OutlineColor;

			inline fixed3 ComputeOutlineColor(fixed3 color) 
			{
			    fixed maxChannel = max(max(color.r, color.g), color.b);
			    fixed3 newColor = color;

			    maxChannel -= (1.0 / 255.0);
			    fixed3 coeff = saturate((newColor.rgb - float3(maxChannel, maxChannel, maxChannel)) * 255.0);
			    newColor.rgb = lerp(0.6 * newColor.rgb, newColor.rgb, coeff);

			    return fixed3(0.8 * newColor.rgb * color.rgb);
			}

			v2f vert(appdata_full v) 
			{
			    v2f o;

			#if USE_SMOOTHEDMESH
			    float3 normal = mul((float3x3) UNITY_MATRIX_IT_MV, v.tangent.xyz);
			#else
			    float3 normal = mul((float3x3) UNITY_MATRIX_IT_MV, v.normal);
			#endif

			    normal.z += 1;

			    // Camera-independent outline size if dist is not 1
			    float4 pos = mul(UNITY_MATRIX_MV, v.vertex);

			#if FIXED_OUTLINESIZE
			    float dist = distance(_WorldSpaceCameraPos, mul(unity_ObjectToWorld, v.vertex));
			#else
			    float dist = 1.0;
			#endif

			    pos = pos + float4(normalize(normal), 0) * _Outline * dist * v.color.a;

			    o.pos = mul(UNITY_MATRIX_P, pos);
			    o.uv = v.texcoord;

			    return o;
			}

			float4 frag(v2f IN): COLOR 
			{
				float luminance = CharacterSunLightColor.a;

			#if USE_AUTOLINECOLOR
			    float3 c = ComputeOutlineColor(tex2D(_MainTex, IN.uv).rgb);
			    return float4(lerp(c.rgb, _OutlineColor.rgb, _OutlineColor.a) * luminance, _Fade);
			#else
				return float4(_OutlineColor.rgb * luminance, _Fade);
			#endif
			}
            ENDCG
        }

        // pass for directional light shading
        Pass 
        {
            Name "FORWARD"

            Tags { "LightMode" = "ForwardBase" }

            Cull Back ZTest LEqual Blend off ZWrite on

            Stencil {
                Ref 128
                WriteMask 128
                Comp Always
                Pass Replace
                ZFail Keep
            }

            CGPROGRAM
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma target 3.0

            #pragma multi_compile_fog
            #pragma multi_compile_fwdbase
            #pragma multi_compile _ USE_DECAL
            #pragma multi_compile USE_RIMLIGHTING _
            #pragma multi_compile USE_SPECULARLIGHTING _
            #pragma multi_compile _ USE_GLOSSYREFLECTION

            #pragma vertex   NPRToonCharacterStandardVS
            #pragma fragment NPRToonCharacterStandardPS
            #include "NPRToonStandard.cginc"
            ENDCG
        }

        // pass for additional light sources
        Pass
        {
        	Name "ADDITIVE"
            Tags { "LightMode" = "ForwardAdd" } 
            Blend One One
     
            CGPROGRAM
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma target 3.0

            #pragma multi_compile_fwdadd

            #pragma vertex   NPRToonCharacterStandardVS
            #pragma fragment NPRToonCharacterStandardPointPS
            #include "NPRToonStandard.cginc" 
            ENDCG
        }
    }

    // for battle
    SubShader
    {
        Tags { "RenderType" = "Opaque" "Queue" = "Geometry+401" }

        LOD 100

        Pass 
        {
        	NAME "FORWARD"

            Tags { "LightMode" = "ForwardBase" }

            Cull Back ZTest LEqual Blend off ZWrite on

            Stencil {
                Ref 128
                WriteMask 128
                Comp Always
                Pass Replace
                ZFail Keep
            }

            CGPROGRAM
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma target 3.0

            #pragma multi_compile_fwdbase
            #pragma multi_compile _ USE_DECAL

            #pragma vertex   NPRToonCharacterStandardVS
            #pragma fragment NPRToonCharacterStandardPS
            #include "NPRToonStandard.cginc"
            ENDCG
        }
    }

    // for reflection
    SubShader 
    {
        Tags { "RenderType" = "Opaque" "Queue" = "Geometry+401" "ForceNoShadowCasting" = "True" }

        LOD 50

        Pass 
        {
            Name "FORWARD"

            Tags { "LightMode" = "Always" }

            LOD 50

            Cull Back ZTest LEqual

            CGPROGRAM
            #pragma vertex   NPRToonOpaqueReflectionVS
            #pragma fragment NPRToonOpaqueReflectionPS

			struct v2f
			{
			    float4 pos : SV_POSITION;
			    float2 uv : TEXCOORD0;
			};

			v2f NPRToonOpaqueReflectionVS(float4 vertex : POSITION, float2 texcoord : TEXCOORD0) 
			{
			    v2f o;
			    UNITY_INITIALIZE_OUTPUT(v2f, o);
			    o.pos = UnityObjectToClipPos(vertex);
			    o.uv.xy = texcoord.xy;
			    return o;
			}

			sampler2D _MainTex;

			fixed4 NPRToonOpaqueReflectionPS(v2f IN, uniform fixed _ReflectionIntensity): SV_Target 
			{
			    return tex2D(_MainTex, IN.uv.xy) * _ReflectionIntensity;
			}
            ENDCG
        }
    }
    
    FallBack "Diffuse"

    CustomEditor "ToonMaterialEditor"
}