// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge Beta 0.36 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.36;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|emission-42-OUT,clip-47-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33624,y:32790,ptlb:TexTex,ptin:_TexTex,tex:14b8d76bfa60c1741bf18337f5c6524a,ntxv:0,isnm:False|UVIN-16-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:3,x:33613,y:33037,ptlb:NoiseTex,ptin:_NoiseTex,tex:9710d323997a9754ead4c87934c48de1,ntxv:0,isnm:False|UVIN-33-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:4,x:33807,y:33295,ptlb:MainTex,ptin:_MainTex,tex:bcfeb92c4307f334a9b92bac0f954976,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:5,x:33602,y:33334|A-4-R,B-4-A;n:type:ShaderForge.SFN_Color,id:6,x:33585,y:32286,ptlb:MainColor,ptin:_MainColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:7,x:33585,y:32442;n:type:ShaderForge.SFN_Time,id:8,x:34576,y:32810;n:type:ShaderForge.SFN_Multiply,id:9,x:34192,y:32719|A-10-OUT,B-8-TSL;n:type:ShaderForge.SFN_ValueProperty,id:10,x:34402,y:32666,ptlb:TexSpd,ptin:_TexSpd,glob:False,v1:3;n:type:ShaderForge.SFN_Frac,id:11,x:33995,y:32746|IN-9-OUT;n:type:ShaderForge.SFN_Panner,id:16,x:33812,y:32746,spu:0,spv:1|UVIN-26-UVOUT,DIST-11-OUT;n:type:ShaderForge.SFN_TexCoord,id:25,x:34576,y:32945,uv:0;n:type:ShaderForge.SFN_Rotator,id:26,x:33995,y:32591|UVIN-25-UVOUT,ANG-27-OUT;n:type:ShaderForge.SFN_ValueProperty,id:27,x:34220,y:32591,ptlb:TexAng,ptin:_TexAng,glob:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:29,x:34282,y:33205|A-8-T,B-40-OUT;n:type:ShaderForge.SFN_Frac,id:31,x:34075,y:33247|IN-29-OUT;n:type:ShaderForge.SFN_Panner,id:33,x:33826,y:33085,spu:0,spv:1|UVIN-35-UVOUT,DIST-31-OUT;n:type:ShaderForge.SFN_Rotator,id:35,x:34053,y:32974|UVIN-25-UVOUT,ANG-37-OUT;n:type:ShaderForge.SFN_ValueProperty,id:37,x:34268,y:33078,ptlb:NoiseAng,ptin:_NoiseAng,glob:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:40,x:34480,y:33237,ptlb:NoiseSpd,ptin:_NoiseSpd,glob:False,v1:3;n:type:ShaderForge.SFN_OneMinus,id:41,x:33371,y:32928|IN-3-RGB;n:type:ShaderForge.SFN_Multiply,id:42,x:33038,y:32811|A-45-OUT,B-41-OUT;n:type:ShaderForge.SFN_Multiply,id:43,x:33204,y:32312|A-6-RGB,B-7-RGB;n:type:ShaderForge.SFN_Multiply,id:44,x:33204,y:32554|A-6-A,B-7-A;n:type:ShaderForge.SFN_Multiply,id:45,x:33310,y:32677|A-43-OUT,B-2-RGB;n:type:ShaderForge.SFN_Multiply,id:46,x:33392,y:33216|A-2-A,B-5-OUT;n:type:ShaderForge.SFN_Clamp01,id:47,x:33008,y:33059|IN-48-OUT;n:type:ShaderForge.SFN_Add,id:48,x:33008,y:33281|A-58-OUT,B-49-OUT;n:type:ShaderForge.SFN_ValueProperty,id:49,x:33275,y:33447,ptlb:ClipAdd,ptin:_ClipAdd,glob:False,v1:0.3;n:type:ShaderForge.SFN_Multiply,id:58,x:33158,y:33170|A-44-OUT,B-46-OUT;proporder:2-10-27-3-37-40-6-4-49;pass:END;sub:END;*/

Shader "Shader Forge/sand_grond" {
    Properties {
        _TexTex ("TexTex", 2D) = "white" {}
        _TexSpd ("TexSpd", Float ) = 3
        _TexAng ("TexAng", Float ) = 0
        _NoiseTex ("NoiseTex", 2D) = "white" {}
        _NoiseAng ("NoiseAng", Float ) = 0
        _NoiseSpd ("NoiseSpd", Float ) = 3
        _MainColor ("MainColor", Color) = (1,1,1,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _ClipAdd ("ClipAdd", Float ) = 0.3
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="Always"
            }
            
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _TexTex; uniform float4 _TexTex_ST;
            uniform sampler2D _NoiseTex; uniform float4 _NoiseTex_ST;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _MainColor;
            uniform float _TexSpd;
            uniform float _TexAng;
            uniform float _NoiseAng;
            uniform float _NoiseSpd;
            uniform float _ClipAdd;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float4 node_7 = i.vertexColor;
                float4 node_8 = _Time + _TimeEditor;
                float node_26_ang = _TexAng;
                float node_26_spd = 1.0;
                float node_26_cos = cos(node_26_spd*node_26_ang);
                float node_26_sin = sin(node_26_spd*node_26_ang);
                float2 node_26_piv = float2(0.5,0.5);
                float2 node_25 = i.uv0;
                float2 node_26 = (mul(node_25.rg-node_26_piv,float2x2( node_26_cos, -node_26_sin, node_26_sin, node_26_cos))+node_26_piv);
                float2 node_16 = (node_26+frac((_TexSpd*node_8.r))*float2(0,1));
                float4 node_2 = tex2D(_TexTex,TRANSFORM_TEX(node_16, _TexTex));
                float2 node_69 = i.uv0;
                float4 node_4 = tex2D(_MainTex,TRANSFORM_TEX(node_69.rg, _MainTex));
                clip(saturate((((_MainColor.a*node_7.a)*(node_2.a*(node_4.r*node_4.a)))+_ClipAdd)) - 0.5);
////// Lighting:
////// Emissive:
                float node_35_ang = _NoiseAng;
                float node_35_spd = 1.0;
                float node_35_cos = cos(node_35_spd*node_35_ang);
                float node_35_sin = sin(node_35_spd*node_35_ang);
                float2 node_35_piv = float2(0.5,0.5);
                float2 node_35 = (mul(node_25.rg-node_35_piv,float2x2( node_35_cos, -node_35_sin, node_35_sin, node_35_cos))+node_35_piv);
                float2 node_33 = (node_35+frac((node_8.g*_NoiseSpd))*float2(0,1));
                float3 emissive = (((_MainColor.rgb*node_7.rgb)*node_2.rgb)*(1.0 - tex2D(_NoiseTex,TRANSFORM_TEX(node_33, _NoiseTex)).rgb));
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    //FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
