﻿Shader "Custom/VertexModifyInSurface"
{
	Properties 
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Amount("Amount", Range(0,5)) = 0 
		_ColorTint("_ColorTint" , Color) = (1,1,1,1)
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }

		CGPROGRAM
		#pragma surface surf Lambert vertex:vert finalcolor:mycolor
		#pragma target 2.0
		#include "UnityCG.cginc"

		struct Input
		{
			float2 uv_MainTex;
			float3 customColor;
		};

		float _Amount;
		float _RuningTime; 
		float4 _ColorTint;

		//void vert(inout appdata_full v)
		//{
		//	//增加随时间变化
		//	v.vertex.xyz += v.normal * _Amount * _SinTime;
		//}

		void vert(inout appdata_full v, out Input o) 
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.customColor = abs(v.normal);
		}

		void mycolor(Input IN, SurfaceOutput o, inout fixed4 color)
		{
			color *= _ColorTint;
		}

		sampler2D _MainTex;
		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * IN.customColor;
		}

		ENDCG
	}
	FallBack "Diffuse"
}
