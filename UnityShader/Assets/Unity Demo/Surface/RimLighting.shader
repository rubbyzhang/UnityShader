﻿Shader "Custom/RimLighting"
{
	Properties
	{
		_RimColor("RimColor", Color) = (1,1,1,1)
		_RimPower("RimPower" , Range(0.5,8.0)) = 3.0 
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("BumpMap" , 2D) = "bump" {}
	}

	SubShader
	{
		Tags{ "Queue" = "Geometry+100" }
		//ZWrite off

		CGPROGRAM
		#include "UnityCG.cginc"

		#pragma surface surf Lambert
		#pragma target 2.0

		sampler2D _MainTex;
		sampler2D _BumpMap;

		float4 _RimColor;
		float  _RimPower;

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 viewDir;
		};

		void surf(Input IN, inout SurfaceOutput o)
		{
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
			float light = dot(normalize(IN.viewDir), o.Normal);
			half rim = 1.0 - saturate(light);
			o.Emission = _RimColor.rgb * pow(rim, _RimPower);
		}
	ENDCG
	}

	FallBack "Diffuse"
}
