// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge Beta 0.36 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.36;sub:START;pass:START;ps:flbk:,lico:0,lgpr:1,nrmq:0,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:False,hqlp:False,tesm:0,blpr:1,bsrc:3,bdst:7,culm:2,dpts:2,wrdp:False,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|emission-5-OUT,alpha-16-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33798,y:32623,ptlb:MainTex,ptin:_MainTex,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:3,x:33678,y:32825,ptlb:NoiseTex,ptin:_NoiseTex,tex:9710d323997a9754ead4c87934c48de1,ntxv:3,isnm:False|UVIN-22-UVOUT;n:type:ShaderForge.SFN_OneMinus,id:4,x:33472,y:32869|IN-3-RGB;n:type:ShaderForge.SFN_Multiply,id:5,x:33187,y:32839|A-15-OUT,B-4-OUT;n:type:ShaderForge.SFN_Color,id:11,x:33787,y:32431,ptlb:MainColor,ptin:_MainColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:12,x:33483,y:32480|A-11-RGB,B-2-RGB;n:type:ShaderForge.SFN_Multiply,id:13,x:33472,y:32623|A-11-A,B-2-A;n:type:ShaderForge.SFN_VertexColor,id:14,x:33391,y:32327;n:type:ShaderForge.SFN_Multiply,id:15,x:33121,y:32393|A-14-RGB,B-12-OUT;n:type:ShaderForge.SFN_Multiply,id:16,x:33121,y:32532|A-14-A,B-13-OUT;n:type:ShaderForge.SFN_Panner,id:22,x:33883,y:32796,spu:0,spv:1|DIST-99-OUT;n:type:ShaderForge.SFN_Time,id:97,x:34524,y:33071;n:type:ShaderForge.SFN_Frac,id:99,x:34076,y:32889|IN-114-OUT;n:type:ShaderForge.SFN_Multiply,id:114,x:34266,y:33033|A-115-OUT,B-97-T;n:type:ShaderForge.SFN_ValueProperty,id:115,x:34512,y:32999,ptlb:Speed,ptin:_Speed,glob:False,v1:1;n:type:ShaderForge.SFN_TexCoord,id:127,x:34371,y:32593,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:130,x:34348,y:32832,ptlb:Ang,ptin:_Ang,glob:False,v1:0;proporder:2-3-11-115-130;pass:END;sub:END;*/

Shader "Shader Forge/sand_alphablend" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _NoiseTex ("NoiseTex", 2D) = "bump" {}
        _MainColor ("MainColor", Color) = (1,1,1,1)
        _Speed ("Speed", Float ) = 1
        _Ang ("Ang", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="Always"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers d3d11 opengl xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _NoiseTex; uniform float4 _NoiseTex_ST;
            uniform float4 _MainColor;
            uniform float _Speed;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_14 = i.vertexColor;
                float2 node_1141 = i.uv0;
                float4 node_2 = tex2D(_MainTex,TRANSFORM_TEX(node_1141.rg, _MainTex));
                float4 node_97 = _Time + _TimeEditor;
                float2 node_22 = (node_1141.rg+frac((_Speed*node_97.g))*float2(0,1));
                float3 emissive = ((node_14.rgb*(_MainColor.rgb*node_2.rgb))*(1.0 - tex2D(_NoiseTex,TRANSFORM_TEX(node_22, _NoiseTex)).rgb));
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,(node_14.a*(_MainColor.a*node_2.a)));
            }
            ENDCG
        }
    }
    
	//FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
