Shader "HL/Transparent/BallBatch"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_Intensity ("Intensity", float) = 1
		_MainTex ("Main Tex", 2D) = "white" {}

		[Enum(UnityEngine.Rendering.BlendOp)] _BlendOp("Blend Op", float) = 0 //Add
		[Enum(UnityEngine.Rendering.BlendMode)] _BlendSrc("Blend Src Factor", float) = 5  //SrcAlpha
		[Enum(UnityEngine.Rendering.BlendMode)] _BlendDst("Blend Dst Factor", float) = 10 //OneMinusSrcAlpha
		[Enum(UnityEngine.Rendering.CullMode)] _CullMode("Cull Mode", float) = 2 //Back
		[HideInInspector][Enum(UnityEngine.Rendering.CompareFunction)] _ZTest("Z Test", float) = 4 //LessEqual
		[HideInInspector][Enum(Off, 0, On, 1)] _ZWrite("Z Write", float) = 0 //Off
	}

		CGINCLUDE
		ENDCG

		SubShader
		{
			Tags { "Queue" = "Transparent" }
			Pass
			{
				Tags { "LIGHTMODE" = "Always" }
				Lighting Off
				Fog { Mode Off }
				BlendOp[_BlendOp]
				Blend[_BlendSrc][_BlendDst]
				Cull[_CullMode]
				ZTest[_ZTest]
				ZWrite[_ZWrite]

				CGPROGRAM
				#include "../HLInclude.cginc"
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma target 2.0
			//only for 
			float4x4 ArrowInfo[4];
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
			};

			v2f vert(appdata i)
			{
				v2f o;
				int2 idx = i.uv1;

				//don't optimize this into targetMatrix=ArrowInfo[idx.x], as sth wrong on android version, here is the right solution, good luck
				float4x4 targetMatrix;
				
				if (idx.x == 0)
				{
					targetMatrix = ArrowInfo[0];
				}
				if (idx.x == 1)
				{
					targetMatrix = ArrowInfo[1];
				}
				if (idx.x == 2)
				{
					targetMatrix = ArrowInfo[2];
				}
				if (idx.x == 3)
				{
					targetMatrix = ArrowInfo[3];
				}

				float4 info = targetMatrix[idx.y];

				float sr = sin(info.w);
				float cr = cos(info.w);

				float2 bias = float2(0.0f, 0.65f);
				float posx = dot(i.vertex.xy*0.3f + bias, float2(cr, -sr));
				float posy = dot(i.vertex.xy*0.3f + bias, float2(sr, cr));
				float4 pos = float4(float2(posx, posy)*info.z + info.xy, 97.5f, 1.0f);

				o.pos = mul(UNITY_MATRIX_VP, pos);
				o.uv = TRANSFORM_TEX(i.uv, _MainTex);
				return o;
			}

			fixed4 frag(v2f i) : COLOR0
			{
				fixed4 color = tex2D(_MainTex, i.uv);
				color.a *= 0.314;
				return color;
			}
	
			ENDCG
		} 
	}
	//Fallback "VertexLit"
}