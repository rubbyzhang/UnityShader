﻿Shader "Custom/SpecularLight" {
	Properties
	 {
		//高光颜色，在Unity BinnPhong内部模型中使用
		_SpecColor ("SpecColor", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SpecPower ("SpecPower", Range(0,1)) = 0.5
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input 
		{
			float2 uv_MainTex;
		}; 

		half _Glossiness;
		half _SpecPower;

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Specular = _SpecPower ;
			o.Gloss = 1.0;
		}

		inline float4 LightingBaseSpecular(SurfaceOutput s , half3 lightDir ,half3 viewDir , half atten)
		{
			float diff = max(0, dot(lightDir , s.Normal)) ; 
			float3 reflectView = reflect(-lightDir , s.Normal) ;
		  //float3 reflectView = normalize(2 * s.Normal * dot( s.Normal , lightDir) - lightDir) ;

			float specDiff = max(0 , dot(reflectView, viewDir)) ;
			float spec     = pow(specDiff , s.Specular * 128) * s.Gloss ;

			float4 col ;
			col.rgb = s.Albedo * _LightColor0.rgb * diff + _SpecColor.rgb * _LightColor0.rgb * spec ;
			//col.rgb =  _SpecColor * _LightColor0.rgb * spec ;
			col.a = s.Alpha ;
			return col ;
		}

		inline float4 LightingHalfSpecular(SurfaceOutput s , half3 lightDir ,half3 viewDir , half atten)
		{
			float diff = max(0, dot(lightDir , s.Normal)) ; 

			float nh   =  normalize(lightDir + viewDir) ;
			float spec =  pow(max(0, dot(nh,s.Normal)) , s.Specular * 128) * s.Gloss ;

			float4 col ;
			col.rgb = s.Albedo * _LightColor0.rgb * diff + _SpecColor.rgb * _LightColor0.rgb * spec ;
			col.a = s.Alpha ;
			return col ;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
