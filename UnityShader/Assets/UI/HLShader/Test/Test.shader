// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "HL/Test/Common"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_Intensity ("Intensity", float) = 1
        _MainTex ("MainTex", 2D) = "white" {}
		_USpeed ("USpeed", Float) = 0
        _VSpeed ("VSpeed", Float) = 0
        _UVRotateSpeed ("UVRotateSpeed", Float) = 0
		//[Enum(Off, 0, On, 1)] _BlendEnable ("Blend Enable", float) = 0
		[Enum(UnityEngine.Rendering.BlendOp)] _BlendOp ("Blend Op", float) = 0 //Add
		[Enum(UnityEngine.Rendering.BlendMode)] _BlendSrc ("Blend Src Factor", float) = 5  //SrcAlpha
		[Enum(UnityEngine.Rendering.BlendMode)] _BlendDst ("Blend Dst Factor", float) = 10 //OneMinusSrcAlpha
		[Enum(UnityEngine.Rendering.BlendMode)] _BlendSrcAlpha ("Blend Src Alpha Factor", float) = 0  //Zero
		[Enum(UnityEngine.Rendering.BlendMode)] _BlendDstAlpha ("Blend Dst Alpha Factor", float) = 1 //One
		[Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", float) = 2 //Back
		[Enum(UnityEngine.Rendering.CompareFunction)] _ZTest("Z Test", float) = 4 //LessEqual
		[Enum(Off, 0, On, 1)] _ZWrite("Z Write", float) = 1 //On
	}

	CGINCLUDE
	//
	ENDCG

	SubShader
	{
		Tags { "Queue" = "Transparent" }
		Pass
		{
			Tags { "LIGHTMODE"="Always" }
			Lighting Off
			Fog { Mode Off }
			//Blend [_BlendEnable] //"Blend Off" or "Blend srcxx dstxx"
			BlendOp [_BlendOp]
			Blend [_BlendSrc] [_BlendDst], [_BlendSrcAlpha] [_BlendDstAlpha]
			Cull [_CullMode]
			ZTest [_ZTest]
			ZWrite [_ZWrite]

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"
	
			float4 _Color;
			float _Intensity;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _USpeed;
			float _VSpeed;
			float _UVRotateSpeed;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
			};
			struct v2f
			{
				float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
			};

			v2f vert(appdata i)
			{
				v2f o;
				o.color = i.color;
				o.pos = UnityObjectToClipPos(i.vertex);
				o.uv = TRANSFORM_TEX(i.uv, _MainTex);

				const half TWO_PI = 3.14159 * 2;
				const half2 VEC_CENTER = half2(0.5h, 0.5h);
				float time = _Time.z;
				float absUVRotSpeed     = abs(_UVRotateSpeed);
				float absUOffsetSpeed   = abs(_USpeed);
				float absVOffsetSpeed   = abs(_VSpeed);
            
				half2 finaluv = o.uv;
				if (absUVRotSpeed > 0)
				{
					finaluv -= VEC_CENTER;
					half rotation = TWO_PI * frac(time * _UVRotateSpeed);
					half sin_rot = sin(rotation);
					half cos_rot = cos(rotation);
					finaluv = half2(
						finaluv.x * cos_rot - finaluv.y * sin_rot,
						finaluv.x * sin_rot + finaluv.y * cos_rot);
					finaluv += VEC_CENTER;
				}
            
				if (absUOffsetSpeed > 0)
				{
					finaluv.x += frac(time * _USpeed);
				}
				if (absVOffsetSpeed > 0)
				{
					finaluv.y += frac(time * _VSpeed);
				}
				o.uv = finaluv;
				return o;
			}

			fixed4 frag(v2f i) : COLOR0
			{
				fixed4 color = _Color * _Intensity * i.color;
				color *= tex2D(_MainTex, i.uv);
				return color;
			}
	
			ENDCG
		} 
	}
	//Fallback "VertexLit"
}