﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Camera))]
public class CustomDepth : MonoBehaviour
{
	public bool IsDefault = true;
	private bool oldIsDefault = false;

	private GameObject depthCamObj;
    private Camera mCam;
    private Shader mCustomDepth;
    private Material mMat;
	public RenderTexture depthTexture;

    private Shader mCopyShader ;
    void Awake()
    {
        mCam = GetComponent<Camera>();
		
		SetMat ();
       // mCam.SetReplacementShader(Shader.Find("Custom/CopyDepth"), "RenderType");
    }

	void SetMat()
	{
		if (false == IsDefault) 
		{
			mCustomDepth = Shader.Find ("Custom/NDCDepthShow");

//			mCopyShader = Shader.Find ("Custom/NDCDepth");

            mCopyShader = Shader.Find ("Custom/CameraDefaultDepth");

		}
		else 
		{
			mCustomDepth = Shader.Find("Custom/CaptureDepthShow_default");
			mCopyShader = Shader.Find("Custom/CaptureDepth_default");
		}

		mMat = new Material(mCustomDepth);

		oldIsDefault = IsDefault;
	}

    internal void OnPreRender()
    {
      /*  if (depthTexture)
        {
            RenderTexture.ReleaseTemporary(depthTexture);
            depthTexture = null;
        }*/

        Camera depthCam;
        if (depthCamObj == null)
        {
            depthCamObj = new GameObject("DepthCamera");
            depthCamObj.AddComponent<Camera>();
            depthCam = depthCamObj.GetComponent<Camera>();
            depthCam.enabled = false;
           // depthCamObj.hideFlags = HideFlags.HideAndDontSave;
        }
        else
        {
            depthCam = depthCamObj.GetComponent<Camera>();
        }

        depthCam.CopyFrom(mCam);
       // depthTexture = RenderTexture.GetTemporary(mCam.pixelWidth, mCam.pixelHeight, 16, RenderTextureFormat.ARGB32);
        depthCam.backgroundColor = new Color(1, 1, 1, 1);
        depthCam.clearFlags = CameraClearFlags.SolidColor; ;
        depthCam.targetTexture = depthTexture;
        depthCam.RenderWithShader(mCopyShader, "RenderType");
        mMat.SetTexture("_DepthTexture", depthTexture);
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
		if (oldIsDefault != IsDefault) {
			SetMat ();		
		}

        if (null != mMat)
        {
            Graphics.Blit(source, destination, mMat);
        }
        else
        {
            Graphics.Blit(source, destination);
        }
    }

}