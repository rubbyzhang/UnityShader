﻿Shader "Custom/NDCDepth" 
{
	Properties 
	{
		_MainTex ("", 2D) = "white" {}
	}


	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		Pass {
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				struct v2f 
				{
				    float4 pos : POSITION;
					//#ifdef UNITY_MIGHT_NOT_HAVE_DEPTH_TEXTURE
				    float2 depth : TEXCOORD0;
					//#endif
				};
				v2f vert( appdata_base v ) 
				{
				    v2f o;
				    o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				    o.depth = o.pos.zw ;
				    return o;
				}
				fixed4 frag(v2f i) : COLOR 
				{
				    float depth = i.depth.x / i.depth.y ;
				    return fixed4(depth,depth,depth,1) * 20;
				}
				ENDCG
			}
	}
}
