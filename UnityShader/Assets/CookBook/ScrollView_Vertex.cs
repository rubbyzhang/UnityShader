﻿using UnityEngine;
using System.Collections;

public class ScrollView_Vertex : MonoBehaviour
{
    public float m_SpeedU = 0.1f;
    public float m_SpeedV = -0.1f;

    public bool IsEnable = false;
    // Update is called once per frame  
    void Update()
    {
        Renderer render = gameObject.GetComponent<Renderer>();
        if (IsEnable && null != render)
        {
            float newOffsetU = Time.time * m_SpeedU;
            float newOffsetV = Time.time * m_SpeedV;
            render.material.mainTextureOffset = new Vector2(newOffsetU, newOffsetV);
        }
    }
}
