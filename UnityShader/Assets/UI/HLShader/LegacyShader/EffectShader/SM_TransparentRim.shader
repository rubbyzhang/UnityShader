// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "ShurikenMagic/TransparentRim" {
	Properties {
		_RimColor ("Rim Color", Color) = (0.5,0.5,0.5,0.5)
		_InnerColor ("Inner Color", Color) = (0.5,0.5,0.5,0.5)
		_InnerColorPower ("Inner Color Power", Range(0.0,1.0)) = 0.5
		_RimPower ("Rim Power", Range(0.0,5.0)) = 2.5
		_AlphaPower ("Alpha Rim Power", Range(0.0,8.0)) = 4.0
		_AllPower ("All Power", Range(0.0, 10.0)) = 1.0
	}

	CGINCLUDE
	#include "UnityCG.cginc"
	
	float4 _RimColor;
	float _RimPower;
	float _AlphaPower;
	float _AlphaMin;
	float _InnerColorPower;
	float _AllPower;
	float4 _InnerColor;

	///*
	struct appdata {
		float4 vertex : POSITION;
		float3 normal : NORMAL;
	};

	struct v2f
	{
		float4 pos : SV_POSITION;
		half3 normal: TEXCOORD1;
		half3 viewDir : TEXCOORD2;
	};

	v2f vert(appdata v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);

		float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
		o.viewDir = normalize(_WorldSpaceCameraPos.xyz - worldPos);
		o.normal = normalize(mul(unity_ObjectToWorld, float4(v.normal,0)).xyz);

		return o;
	}

	fixed4 frag(v2f i) : COLOR0
	{
		half rim = 1.0 - saturate(dot (i.viewDir, i.normal));
		half3 color = _RimColor.rgb * pow (rim, _RimPower)*_AllPower+(_InnerColor.rgb*2*_InnerColorPower);
		half a = (pow (rim, _AlphaPower))*_AllPower;
		return fixed4(color, a);
	}
	//*/

	/*
	//move calcul to vert shader
	struct appdata {
		float4 vertex : POSITION;
		float3 normal : NORMAL;
	};
	struct v2f
	{
		float4 pos : SV_POSITION;
		float4 color : COLOR;
	};

	v2f vert(appdata v)
	{
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

		float3 worldPos = mul(_Object2World, v.vertex).xyz;
		half3 viewDir = normalize(_WorldSpaceCameraPos.xyz - worldPos);
		half3 normal = normalize(mul(_Object2World, float4(v.normal,0)).xyz);
		
		half rim = 1.0 - saturate(dot (viewDir, normal));
		half3 color = _RimColor.rgb * pow (rim, _RimPower)*_AllPower+(_InnerColor.rgb*2*_InnerColorPower);
		half a = (pow (rim, _AlphaPower))*_AllPower;
		o.color = fixed4(color, a);
		return o;
	}
	fixed4 frag(v2f i) : COLOR0
	{
		return fixed4(i.color);
	}
	*/

	ENDCG

	SubShader {
	Tags { "Queue" = "Transparent" }
	Pass {
			Tags { "LIGHTMODE"="Always" }
			Lighting Off
			Cull Back
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Fog { Mode Off }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma only_renderers d3d9 gles

			ENDCG
		} 
	}
	//Fallback "VertexLit"
} 



/*SubShader {
Tags { "Queue" = "Transparent" }

CGPROGRAM
#pragma surface surf Lambert alpha
struct Input {
float3 viewDir;
INTERNAL_DATA
};
float4 _RimColor;
float _RimPower;
float _AlphaPower;
float _AlphaMin;
float _InnerColorPower;
float _AllPower;
float4 _InnerColor;
void surf (Input IN, inout SurfaceOutput o) {
half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
o.Emission = _RimColor.rgb * pow (rim, _RimPower)*_AllPower+(_InnerColor.rgb*2*_InnerColorPower);
o.Alpha = (pow (rim, _AlphaPower))*_AllPower;
}
ENDCG
}
Fallback "VertexLit"
} */