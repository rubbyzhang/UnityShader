// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge Beta 0.36 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.36;sub:START;pass:START;ps:flbk:,lico:0,lgpr:1,nrmq:1,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,ufog:False,aust:False,igpj:True,qofs:0,qpre:0,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32651,y:32591|emission-54-OUT,alpha-63-OUT;n:type:ShaderForge.SFN_Tex2d,id:3,x:33624,y:32668,ptlb:MainTex,ptin:_MainTex,tex:dfafb03ff055fb448b694546084f8113,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:33,x:33332,y:32425|A-35-RGB,B-3-RGB;n:type:ShaderForge.SFN_Color,id:35,x:33606,y:32352,ptlb:MainColor,ptin:_MainColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:36,x:33332,y:32581|A-35-A,B-3-A;n:type:ShaderForge.SFN_VertexColor,id:53,x:33352,y:32749;n:type:ShaderForge.SFN_Multiply,id:54,x:33077,y:32546|A-33-OUT,B-53-RGB;n:type:ShaderForge.SFN_Multiply,id:55,x:33063,y:32729|A-36-OUT,B-53-A;n:type:ShaderForge.SFN_Multiply,id:61,x:33046,y:32961|A-55-OUT,B-62-OUT;n:type:ShaderForge.SFN_ValueProperty,id:62,x:33312,y:33024,ptlb:A_str,ptin:_A_str,glob:False,v1:1;n:type:ShaderForge.SFN_Clamp01,id:63,x:32885,y:32858|IN-61-OUT;proporder:3-35-62;pass:END;sub:END;*/

Shader "Shader Forge/rushline_black" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _MainColor ("MainColor", Color) = (1,1,1,1)
        _A_str ("A_str", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Background"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="Always"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _MainColor;
            uniform float _A_str;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float2 node_67 = i.uv0;
                float4 node_3 = tex2D(_MainTex,TRANSFORM_TEX(node_67.rg, _MainTex));
                float4 node_53 = i.vertexColor;
                float3 emissive = ((_MainColor.rgb*node_3.rgb)*node_53.rgb);
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,saturate((((_MainColor.a*node_3.a)*node_53.a)*_A_str)));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
