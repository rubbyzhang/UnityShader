Shader "BlueWar/NPRLitToonEnvNoOutline"
{
    Properties
    {
        [HideInInspector]_Fade("Fade", Range(0, 1)) = 1.0

        _LightThreshold("Light Threshold", Range(0, 1)) = 0.5
        _LightSmoothness("Light Smoothness", Range(0, 1)) = 0.5
        _GlowPower("Glow Power", Range(0, 1)) = 0
        _GlowColor("Glow Color", Color) = (1, 1, 1, 1)
		_GlossyCube("Glossy Reflection Cube", CUBE) = "" {}
        _MainTex("Base (RGB)", 2D) = "white" {}
        _DecalTex("Decal (RGB)", 2D) = "black" {}
        _ToonMap("ToonMap (RGB)", 2D) = "white" {}
        _ShadingMask("ShadingMask (RGB)", 2D) = "white" {}
        SpecularPower("SpecularPower", Float) = 1.0
        SpecularColor("SpecularColor", Color) = (0.0, 0.0, 0.0, 0.0)
        SpecularFactor("SpecularFactor", Float) = 1.0
        RimGeneral("Rim General", Range(0, 1)) = 0.5
        RimPower("Rim Power", Float) = 1
        RimColor("RimColor", Color) = (1, 1, 1, 1)
        DecalUV("DecalUV", Vector) = (1, 1, 1, 1) //center_xy[0,0]-[1,1], tile_zw,[0,+INF] 

        _OcclusionRimColor("Occlusion Rim Color", Color) = (0, 1, 1, 1)
        _OcclusionRimPower("Occlusion Rim Power", Range(0.1, 8.0)) = 2

        [Toggle(USE_DECAL)] _UseDecal("Use Decal?", Float) = 0
        [Toggle(USE_SPECULARLIGHTING)] _UseSpecularLighting("Use Specular Lighting?", Float) = 1
        [Toggle(USE_RIMLIGHTING)] _UseRimLighting("Use Rim Lighting?", Float) = 1
        [Toggle(USE_GLOSSYREFLECTION)] _UseGlossyReflection("Use Glossy Reflection?", Float) = 1
    }

    SubShader 
    {
        Tags { "RenderType" = "Opaquer" "Queue" = "Geometry+401" }

        LOD 150

        // USEPASS "BlueWar/NPRLitToonEnv/XRAY"

        // ---- forward rendering base pass:
        Pass 
        {
            Name "FORWARD"
            Tags { "LightMode" = "ForwardBase" }

            Cull Back ZTest LEqual Blend off ZWrite on

            Stencil {
                Ref 128
                WriteMask 128
                Comp Always
                Pass Replace
                ZFail Keep
            }

            CGPROGRAM
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma target 3.0

            #pragma multi_compile_fog
            #pragma multi_compile_fwdbase
            #pragma multi_compile _ USE_DECAL
            #pragma multi_compile USE_RIMLIGHTING _
            #pragma multi_compile USE_SPECULARLIGHTING _
            #pragma multi_compile _ USE_GLOSSYREFLECTION

			#pragma vertex   NPRToonCharacterStandardVS
            #pragma fragment NPRToonCharacterStandardPS
            #include "NPRToonStandard.cginc"
            ENDCG
        }
    }

    // for battle
    SubShader 
    {
        Tags { "RenderType" = "Opaquer" "Queue" = "Geometry+401" }

        LOD 100

        // USEPASS "BlueWar/NPRLitToonEnv/XRAY"
		USEPASS "BlueWar/NPRLitToonEnv/FORWARD"
    }

    //this subshader's for reflection
    SubShader 
    {
        Tags { "RenderType" = "Opaque" "Queue" = "Geometry+401" "ForceNoShadowCasting" = "True" }
        LOD 50
        USEPASS "BlueWar/NPRLitToonEnv/FORWARD"
    }
    
    FallBack "Diffuse"

    CustomEditor "ToonMaterialEditor"
}