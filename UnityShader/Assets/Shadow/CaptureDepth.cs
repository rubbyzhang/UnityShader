﻿using UnityEngine;
using System.Collections;

public class CaptureDepth : MonoBehaviour
{
	public RenderTexture depthTexture;

	private Camera mCam;
	private Shader mSampleDepthShader ;

	void Start()
	{
		Debug.Log ("___________________CaptureDepth");

		mCam = GetComponent<Camera>();
		mSampleDepthShader = Shader.Find("ShadowMap/CaptureDepth");
		if (mSampleDepthShader == null)
		{
			Debug.Log ("___________________");
		}

		if (mCam != null) 
		{
			mCam.backgroundColor = Color.white;
			mCam.clearFlags = CameraClearFlags.Color; ;
			mCam.targetTexture = depthTexture;
			mCam.orthographic = true;
		
			mCam.enabled = false;

			Shader.SetGlobalTexture ("_LightDepthTex", depthTexture);

			Debug.Log ("RenderWithShader");

			mCam.RenderWithShader(mSampleDepthShader, "RenderType");
		}
	}
}