﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/CameraDepthNormals" 
{
Properties 
{
   _MainTex ("", 2D) = "white" {}
}

SubShader 
{
	//Tags { "RenderType"="Opaque" }

	Pass
	{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		sampler2D _CameraDepthNormalsTexture;

        struct uinput
        {
            float4 vertex : POSITION;
            half2 uv : TEXCOORD0;
        };

		struct v2f 
		{
		   float4 pos : SV_POSITION;
		   float2 uv : TEXCOORD0;
		   float4 scrPos: TEXCOORD1;
		};

		//Our Vertex Shader
		v2f vert (uinput v)
		{
		   v2f o;
		   o.pos = UnityObjectToClipPos (v.vertex);
		   o.scrPos  = ComputeScreenPos(o.pos);
		   o.scrPos.y =  o.scrPos.y;
   		   o.uv =  v.uv;
		 //  o.uv.y = 1- o.uv.y ;

		   return o;
		}

		sampler2D _MainTex; 

		//Our Fragment Shader
		half4 frag (v2f i) : COLOR
		{

			float3 normalValues;
			float depthValue;

			//DecodeDepthNormal(tex2D(_CameraDepthNormalsTexture, i.scrPos.xy), depthValue, normalValues);

			DecodeDepthNormal(tex2D(_CameraDepthNormalsTexture, i.uv), depthValue, normalValues);


			depthValue = depthValue * 20.0f ;

			float4 depth = float4(depthValue,depthValue,depthValue,1);
			return depth;
		}

		ENDCG
	}
}
FallBack "Diffuse"
}