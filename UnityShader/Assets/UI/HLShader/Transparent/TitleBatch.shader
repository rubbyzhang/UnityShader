Shader "HL/Transparent/TitleBatch"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_Intensity ("Intensity", float) = 1
		FontTex ("FontTex", 2D) = "white" {}
	}

		CGINCLUDE
		ENDCG

		SubShader
	{
		Tags { "Queue" = "Transparent" }
		Pass
		{
			Tags { "LIGHTMODE" = "Always" }
			Cull Off
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#include "../HLInclude.cginc"
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma target 2.0

			sampler2D FontTex;
			//only for 
			float4x4 TitleInfo[4];
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
			};

			v2f vert(appdata i)
			{
				v2f o;
				int2 idx = i.uv1;

				//don't optimize this into targetMatrix=TitleInfo[idx.x], as sth wrong on android version, here is the right solution, good luck
				float4x4 targetMatrix;
				
				if (idx.x == 0)
				{
					targetMatrix = TitleInfo[0];
				}
				if (idx.x == 1)
				{
					targetMatrix = TitleInfo[1];
				}
				if (idx.x == 2)
				{
					targetMatrix = TitleInfo[2];
				}
				if (idx.x == 3)
				{
					targetMatrix = TitleInfo[3];
				}

				float4 info = targetMatrix[idx.y];
				float4 pos = float4(i.vertex.xy * info.w*0.2f + info.xy, 113.5f, 1.0f);

				o.pos = mul(UNITY_MATRIX_VP, pos);
				o.uv = i.uv;
				return o;
			}

			fixed4 frag(v2f i) : COLOR0
			{
				fixed4 data = tex2D(FontTex, i.uv);
				return data.a;
			}
	
			ENDCG
		} 
	}
	//Fallback "VertexLit"
}