﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/VPos"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct v2f
			{
				float2 uv : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (
					float2 uv : TEXCOORD0,
					float4 vertex : POSITION,
					out float4 outpos : SV_POSITION 
					 )
			{
				outpos = UnityObjectToClipPos(vertex);

				v2f o;
				o.uv = TRANSFORM_TEX(uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i , UNITY_VPOS_TYPE screenPos : VPOS) : SV_Target
			{
				screenPos.xy  = floor(screenPos.xy * 0.1) * 0.5;
				float checker = -frac(screenPos.x + screenPos.y);
				clip(checker) ;
				fixed4 col = tex2D(_MainTex, i.uv);
				return col;
			}
			ENDCG
		}
	}
}
