﻿Shader "Custom/ScrollViewUV_Light" 
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_Shininess("Shininess", Range(0, 1)) = 0.7
		_Gloss("Gross",Range(0,2)) = 1 

		_MainTex("Base (RGB)", 2D) = "white" {}
		_NormalTex("Normal", 2D) = "white" {}

		_ScrollXSpeed("XSpeed", Range(0,10)) = 0
		_ScrollYSpeed("YSpeed", Range(0,10)) = 0.3
	}

	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}  

		//ZWrite on
		//AlphaTest Greater 0 
		//Blend SrcAlpha OneMinusSrcAlpha

		//Pass
		//{
		//	Tags {"LightMode"="ForwardBase"}

		//	CGPROGRAM
		//	#pragma vertex vert
		//	#pragma fragment frag
		//	#pragma target 2.0
		//	#include "UnityCG.cginc"
		//	#include "Lighting.cginc"

		//	struct appdata
		//	{
		//		float4 vertex  : POSITION ;
		//		float2 uv : TEXCOORD0;
		//		float2 normal_uv : TEXCOORD1;
		//		float3 worldPos : TEXCOORD2 ;
		//	};

		//	struct v2f
		//	{
		//		float2 uv : TEXCOORD0;
		//		float2 normal_uv : TEXCOORD1;
		//		float3 worldPos : TEXCOORD2 ;
		//		float4 vertex  : SV_POSITION ;
		//	};

		//	float4 _Color ;
		//	float _Shininess ;
		//	float _Gross ;

		//	sampler2D _MainTex ;
		//	sampler2D _NormalTex;

		//	float _ScrollXSpeed ;
		//	float _ScrollYSpeed ;

		//	v2f vert(appdata v)
		//	{
		//		v2f o;
		//		o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
		//		o.uv.xy   = v.uv + float2(_ScrollXSpeed , _ScrollYSpeed) * _Time.xy;
		//		//o.normal_uv = v.normal_uv ;
		//		o.normal_uv = v.normal_uv + float2(_ScrollXSpeed , _ScrollYSpeed) * _Time.xy;;
		//		o.worldPos = mul(_Object2World,v.vertex) ;
		//		return o;
		//	}

		//	float4 frag(v2f i): SV_Target
		//	{
		//		//texture
		//		float4 texCol       =   tex2D(_MainTex , i.uv);
		//		float3 worldNormal  =  tex2D(_NormalTex,i.normal_uv) ;
		//		float3 lightDir     =   _WorldSpaceLightPos0.xyz ;

		//		//diffuse
		//		float diff = max(0,dot(worldNormal,lightDir)) * 0.5 + 0.5  ;
		//		float3 diffColor =  texCol.rgb * _LightColor0.rgb * diff;

		//		float3 worldView = i.worldPos - _WorldSpaceCameraPos;
		//		float  dotSpecular = dot(worldNormal,  normalize(worldView + lightDir));
		//		float specFactor =  pow(max(0,dotSpecular),_Shininess * 128) ;
		//		float3 specColor =   texCol.rgb * specFactor; 
		//		float4 col ;
		//		col.rgb =    specColor   ;
		//		col.a   =  texCol.a ;
		//		return col ;
		//	}
		//	ENDCG
		//}


		CGPROGRAM
		#pragma surface surf SimpleWater
		#pragma target 2.0

		struct Input
		{
			float2 uv_MainTex ;
			float2 uv_NormalTex ;
		};

		float4 _Color ;
		float _Shininess ;
		float _Gloss ;

		sampler2D _MainTex ;
		sampler2D _NormalTex;

		float _ScrollXSpeed ;
		float _ScrollYSpeed ;

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float2 mainUV  = IN.uv_MainTex + float2(_ScrollXSpeed, _ScrollYSpeed ) * _Time.xy ;
			fixed4 c1      = tex2D(_MainTex ,mainUV) ;

			float2 normalUV  = IN.uv_NormalTex + float2(_ScrollXSpeed, _ScrollYSpeed )* _Time.xy ;
			fixed3 normal    = UnpackNormal(tex2D(_NormalTex ,normalUV));

			o.Albedo = c1.rgb  ;
			o.Alpha = c1.a ;
			o.Normal = UnpackNormal(tex2D(_NormalTex ,normalUV));
			o.Gloss = _Gloss;
		}

		inline half4 LightingSimpleWater(SurfaceOutput s , half3 lightDir , half3 viewDir , half atten)
		{
			half3 h = normalize(viewDir + lightDir) ;
			half diff = max(0 , dot(lightDir , s.Normal)) ;
			float hn = max(0 , dot(h , s.Normal)) ;
			float spec = pow(hn, _Shininess * 128) * _Gloss;

			half4 col ;
			col.rgb = s.Albedo * _LightColor0.rgb * diff     +     _Color * _LightColor0.rgb  * spec ;
			col.a = s.Alpha ;
			return col ;	
		}
		ENDCG
	}
		FallBack "Diffuse"
}

