﻿Shader "Custom/ColorController" 
{
	Properties 
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SrcStartValue("Src Start Value" , Range(0,255)) = 0.1 
		_SrcEndValue("Src End Value" , Range(0,255)) = 0.2

		_TargetStartValue("Target Start Value" , Range(0,255)) = 0.3
		_TargetEndValue("Target End Value" , Range(0,255)) = 0.4

		_Gamma("Gamma" , Range(0,1)) = 0.1 
	}


	//颜色线性拉伸，在图像中还有很多其他的方法
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 2.0

		sampler2D _MainTex;
		float _SrcStartValue ;
		float _SrcEndValue ;
		float _Gamma ;
		float _TargetStartValue ;
		float _TargetEndValue ;

		struct Input 
		{
			float2 uv_MainTex;
		};

		float GetPixelLevel(float color)
		{
			float outColor ;
			outColor = max(0, color*255 - _SrcStartValue);
			outColor =  saturate(pow(outColor / (_SrcEndValue - _SrcStartValue) , _Gamma) ) ;
			outColor = outColor *  (_TargetEndValue - _TargetStartValue) + _TargetStartValue ;
			return outColor / 255;
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) ; 
			o.Albedo = float3(GetPixelLevel(c.r) , GetPixelLevel(c.g) , GetPixelLevel(c.b));
			o.Alpha = c.a;
		}


		ENDCG
	}
	FallBack "Diffuse"
}
